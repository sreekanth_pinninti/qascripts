
begin tran
if not exists (select * from SecureMessageTemplates with (updlock,serializable) where TemplateId = 'transaction.general')
begin
INSERT [dbo].[SecureMessageTemplates] ([InsertAt], [UpdateAt], [ExpireAt], [ExtendData], [TemplateId], [Category], [Subject], [Body], [AllowedCategories], [ReadOnlyBody]) VALUES (GetDate(), GetDate(), NULL, NULL, N'transaction.general', N'Transaction Inquiry', N'', N'', N'Transaction Inquiry,Transaction Dispute', N'Transaction date: {transactionDate}
Description: {transactionDescription}
Amount: {transactionAmount}
-----------------------------------------------
Account name: {subAccountName}
Account type: {subAccountType}
Account number: {subAccountNumber}
Account balance: {subAccountBalance}')
end
commit tran

begin tran
if not exists (select * from SecureMessageTemplates with (updlock,serializable) where TemplateId = 'subaccount.general')
begin
INSERT [dbo].[SecureMessageTemplates] ([InsertAt], [UpdateAt], [ExpireAt], [ExtendData], [TemplateId], [Category], [Subject], [Body], [AllowedCategories], [ReadOnlyBody]) VALUES (GetDate(), GetDate(), NULL, NULL, N'subaccount.general', N'Account Inquiry', N'', N'', N'Account Inquiry', N'Account name: {subAccountName}
Account type: {subAccountType}
Account number: {subAccountNumber}
Account balance: {subAccountBalance}')
end
commit tran

begin tran
if not exists (select * from SecureMessageTemplates with (updlock,serializable) where TemplateId = 'checkimage.service.unavailable')
begin
INSERT [dbo].[SecureMessageTemplates] ([InsertAt], [UpdateAt], [ExpireAt], [ExtendData], [TemplateId], [Category], [Subject], [Body], [AllowedCategories], [ReadOnlyBody]) VALUES (GetDate(), GetDate(), NULL, NULL, N'checkimage.service.unavailable', N'Check Image', N'Check Image Service Not Available', N'Customer Support:

I was looking for a check image and the service was not available. I would like for you to look to see if it is available for me at all, please.

Thank you,
{fullname}', N'Check Image', N'Transaction date: {transactionDate}
Description: {transactionDescription}
Amount: {transactionAmount}
Check Number: {transactionCheckNumbers}
-----------------------------------------------
Account Name: {subAccountName}
Account Type: {subAccountType}
Account Number: {subAccountNumber}
Account Balance: {subAccountBalance}')
end
commit tran

begin tran
if not exists (select * from SecureMessageTemplates with (updlock,serializable) where TemplateId = 'checkimage.image.missing')
begin
INSERT [dbo].[SecureMessageTemplates] ([InsertAt], [UpdateAt], [ExpireAt], [ExtendData], [TemplateId], [Category], [Subject], [Body], [AllowedCategories], [ReadOnlyBody]) VALUES (GetDate(), GetDate(), NULL, NULL, N'checkimage.image.missing', N'Check Image', N'Check Image Request', N'Customer Support:

I was looking for a check image and it was not available online. I would like for you to look to see if it is available for me at all, please.

Thank you,
{fullname}', N'Check Image', N'Transaction date: {transactionDate}
Description: {transactionDescription}
Amount: {transactionAmount}
Check Number: {transactionCheckNumbers}
-----------------------------------------------
Account Name: {subAccountName}
Account Type: {subAccountType}
Account Number: {subAccountNumber}
Account Balance: {subAccountBalance}')
end
commit tran