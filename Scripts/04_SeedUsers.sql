Declare @MultiAccountUserId varchar(1000), @MultiAccountUsername varchar (1000), @UserId varchar(1000), @Username varchar(1000), @Passwordhash varchar(1000), @PasswordHistory varchar(max), @AccountId bigint, @ConcurrencyStamp varchar(1000), @PortfolioId INT

Set @MultiAccountUserId = '8CCF2E02-E42A-45C3-B00C-7C66622B64BB'
Set @MultiAccountUsername = 'multiaccount1'
Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179d4'
Set @Username = 'Sample1'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659665'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123


-- Seed DigitalUser

begin tran

select @PortfolioId = Id from Portfolios WHERE Identifier = '111223333'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

select @PortfolioId = Id from Portfolios WHERE Identifier = '999880000'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@MultiAccountUsername)))
begin
    insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
    values (@MultiAccountUserId, GetDate(), GetDate(), @MultiAccountUserName, @PasswordHash, Upper(@MultiAccountUserName),  @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)
	
	update Portfolios set OwnerUserId = @MultiAccountUserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179f0'
Set @Username = 'auto_payments'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659680'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000001'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179d7'
Set @Username = 'auto_transfers'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659667'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000002'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179d8'
Set @Username = 'Auto_Accountsettings'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659668'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000003'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179d9'
Set @Username = 'Auto_Stoppayment'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659669'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000004'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e0'
Set @Username = 'Auto_Scheduledpayments'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659670'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000005'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e1'
Set @Username = 'Auto_Profile'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659671'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000006'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e2'
Set @Username = 'Auto_Billpay'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659672'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000007'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e3'
Set @Username = 'Auto_Changepassword'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659673'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000008'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e4'
Set @Username = 'Auto_Changeusername'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659674'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000009'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e5'
Set @Username = 'Auto_Scheduledtransfers'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659675'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000010'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e6'
Set @Username = 'Auto_Troublesigningin'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659676'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000011'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680179e7'
Set @Username = 'auto_acctlock'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659677'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

select @PortfolioId = Id from Portfolios WHERE Identifier = '000000012'
if (not exists (select * from DigitalUsers with (updlock,serializable) where NormalizedUsername = Upper(@Username)) and (@PortfolioId > 0))
begin
   insert into DigitalUsers (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, Type, PortfolioId, PasswordChangedUtc, Status, PasscodeFailedCount, PasscodeFailedCountTotal)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, 0, @PortfolioId, GetDate(), 1, 0, 0)

   update Portfolios set OwnerUserId = @UserId where Id = @PortfolioId
end

commit tran

-- Seed DigitalAdmin
Set @UserId = '41d80b69-cfed-41d9-9cc0-2d51680279d4'
Set @Username = 'testAdmin'
Set @Passwordhash = 'AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==' -- password123
Set @PortfolioId = 0
Set @ConcurrencyStamp = 'a99ceb73-8f2b-49b5-a3c3-120ea5659665'
Set @PasswordHistory = '[{"PasswordHash":"AQAAAAEAACcQAAAAEIEbibhWvURRMYlFcMLWoInIgEbLyG1xjLhBooBSvG9+hFtHWddk7sP9onGQywUIwA==","ChangedTimestampUtc":"2019-05-20T18:58:03.1438601Z"}]' -- password123

begin tran
if not exists (select * from DigitalAdmins with (updlock,serializable) where NormalizedUsername = Upper(@Username))
begin
   insert into DigitalAdmins (Id, InsertAt, UpdateAt, Username, PasswordHash, NormalizedUsername, PasswordHistory, Concurrencystamp, PasswordChangedUtc)
   values (@UserId, GetDate(), GetDate(), @Username, @Passwordhash, Upper(@Username), @PasswordHistory, @ConcurrencyStamp, GetDate())
end
commit tran