--Seed Activity Labels

Declare @ModuleName varchar(1000), @ActionName varchar(1000), @ModuleDisplayName varchar(1000), @ActionDisplayName varchar(1000)

Set @ModuleName = 'Access'
Set @ActionName = 'Login'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Login'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Access'
Set @ActionName = 'Logout'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Logout'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Access'
Set @ActionName = 'RegisterTerms'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Register Terms'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Access'
Set @ActionName = 'Unlock'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Unlock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Access'
Set @ActionName = 'Lock'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Lock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'API'
Set @ActionName = 'Unauthorized Access'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Denied'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'BiometricsLogin'
Set @ActionName = 'Disabled'
Set @ModuleDisplayName = 'Biometrics'
Set @ActionDisplayName = 'Disabled'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'BiometricsLogin'
Set @ActionName = 'Enabled'
Set @ModuleDisplayName = 'Biometrics'
Set @ActionDisplayName = 'Enabled'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'CheckImages'
Set @ActionName = 'GetImages'
Set @ModuleDisplayName = 'Check Image'
Set @ActionDisplayName = 'Retreive'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Credentials'
Set @ActionName = 'ChangePassword'
Set @ModuleDisplayName = 'Password'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Credentials'
Set @ActionName = 'ChangeUsername'
Set @ModuleDisplayName = 'Username'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Device'
Set @ActionName = 'Added'
Set @ModuleDisplayName = 'Device'
Set @ActionDisplayName = 'Added'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Device'
Set @ActionName = 'Updated'
Set @ModuleDisplayName = 'Device'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'AddressAdded'
Set @ModuleDisplayName = 'Address'
Set @ActionDisplayName = 'Added'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'EmailAddressAdded'
Set @ModuleDisplayName = 'Email'
Set @ActionDisplayName = 'Added'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'PhoneAdded'
Set @ModuleDisplayName = 'Phone'
Set @ActionDisplayName = 'Added'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'AddressUpdated'
Set @ModuleDisplayName = 'Address'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'EmailAddressUpdated'
Set @ModuleDisplayName = 'Email'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'PhoneUpdated'
Set @ModuleDisplayName = 'Phone'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'SessionTimeoutEdited'
Set @ModuleDisplayName = 'Session Timeout'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'AddressDeleted'
Set @ModuleDisplayName = 'Address'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'EmailAddressDeleted'
Set @ModuleDisplayName = 'Email'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Profile'
Set @ActionName = 'PhoneDeleted'
Set @ModuleDisplayName = 'Phone'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SecureMessage'
Set @ActionName = 'Sent'
Set @ModuleDisplayName = 'Message'
Set @ActionDisplayName = 'Sent'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SecureMessage'
Set @ActionName = 'Deleted'
Set @ModuleDisplayName = 'Message'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SecureMessage'
Set @ActionName = 'Read'
Set @ModuleDisplayName = 'Message'
Set @ActionDisplayName = 'Read'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'StatementSettings'
Set @ActionName = 'Changed'
Set @ModuleDisplayName = 'Statement Settings'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'StopPayment'
Set @ActionName = 'Created'
Set @ModuleDisplayName = 'Stop Payment'
Set @ActionDisplayName = 'Created'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'StopPayment'
Set @ActionName = 'Edited'
Set @ModuleDisplayName = 'Stop Payment'
Set @ActionDisplayName = 'Edited'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'StopPayment'
Set @ActionName = 'Deactivated'
Set @ModuleDisplayName = 'Check Stop'
Set @ActionDisplayName = 'Deactivated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SubAccountSettings'
Set @ActionName = 'Change'
Set @ModuleDisplayName = 'Account Settings'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Transactions'
Set @ActionName = 'Export'
Set @ModuleDisplayName = 'Transaction'
Set @ActionDisplayName = 'Export'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Transfer'
Set @ActionName = 'Deleted'
Set @ModuleDisplayName = 'Transfer'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Transfer'
Set @ActionName = 'Edited'
Set @ModuleDisplayName = 'Transfer'
Set @ActionDisplayName = 'Edited'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Transfer'
Set @ActionName = 'One-Time'
Set @ModuleDisplayName = 'Transfer'
Set @ActionDisplayName = 'One-Time'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Transfer'
Set @ActionName = 'Scheduled'
Set @ModuleDisplayName = 'Transfer'
Set @ActionDisplayName = 'Scheduled'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'UserAnnouncements'
Set @ActionName = 'MarkRead Announcement'
Set @ModuleDisplayName = 'UserAnnouncement'
Set @ActionDisplayName = 'User Read'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'UserAnnouncements'
Set @ActionName = 'Dismiss Announcement'
Set @ModuleDisplayName = 'UserAnnouncement'
Set @ActionDisplayName = 'User Dismissed'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'UserAnnouncements'
Set @ActionName = 'Deleted Announcement'
Set @ModuleDisplayName = 'UserAnnouncement'
Set @ActionDisplayName = 'User Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Configuration'
Set @ActionName = 'Edited'
Set @ModuleDisplayName = 'Configuration'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'User'
Set @ActionName = 'Allow Self Unlock'
Set @ModuleDisplayName = 'User'
Set @ActionDisplayName = 'Allow Self Unlock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'User'
Set @ActionName = 'Lock'
Set @ModuleDisplayName = 'User'
Set @ActionDisplayName = 'Lock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'User'
Set @ActionName = 'Unlock'
Set @ModuleDisplayName = 'User'
Set @ActionDisplayName = 'Unlock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'User'
Set @ActionName = 'Delete'
Set @ModuleDisplayName = 'User'
Set @ActionDisplayName = 'Delete'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'AdminUser'
Set @ActionName = 'Lock'
Set @ModuleDisplayName = 'Admin User'
Set @ActionDisplayName = 'Lock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'AdminUser'
Set @ActionName = 'Unlock'
Set @ModuleDisplayName = 'Admin User'
Set @ActionDisplayName = 'Unlock'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'AdminUser'
Set @ActionName = 'ResetPassword'
Set @ModuleDisplayName = 'Admin User'
Set @ActionDisplayName = 'Reset Password'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'AdminUser'
Set @ActionName = 'Deleted'
Set @ModuleDisplayName = 'Admin User'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'AdminUser'
Set @ActionName = 'Edited'
Set @ModuleDisplayName = 'Admin User'
Set @ActionDisplayName = 'Edited'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Role'
Set @ActionName = 'SetAdmin'
Set @ModuleDisplayName = 'Permissions'
Set @ActionDisplayName = 'Set Admin Role'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Role'
Set @ActionName = 'SetUser'
Set @ModuleDisplayName = 'Permissions'
Set @ActionDisplayName = 'Set User Role'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Role'
Set @ActionName = 'Create'
Set @ModuleDisplayName = 'Permissions'
Set @ActionDisplayName = 'Create Role'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Role'
Set @ActionName = 'Edit'
Set @ModuleDisplayName = 'Permissions'
Set @ActionDisplayName = 'Edit Role'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Role'
Set @ActionName = 'Delete'
Set @ModuleDisplayName = 'Permissions'
Set @ActionDisplayName = 'Delete Role'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Metadata'
Set @ActionName = 'DefinitionsChanged'
Set @ModuleDisplayName = 'Products'
Set @ActionDisplayName = 'Update Metadata'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Announcements'
Set @ActionName = 'Created Announcement'
Set @ModuleDisplayName = 'Announcement'
Set @ActionDisplayName = 'Created'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Announcements'
Set @ActionName = 'Published Announcement'
Set @ModuleDisplayName = 'Announcement'
Set @ActionDisplayName = 'Published'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Announcements'
Set @ActionName = 'Deactivate Announcement'
Set @ModuleDisplayName = 'Announcement'
Set @ActionDisplayName = 'Deactivated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Announcements'
Set @ActionName = 'Updated Announcement'
Set @ModuleDisplayName = 'Announcement'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Announcements'
Set @ActionName = 'Deleted Announcement'
Set @ModuleDisplayName = 'Announcement'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'TroubleSigningIn'
Set @ActionName = 'PasswordResetStarted'
Set @ModuleDisplayName = 'Trouble Signing In'
Set @ActionDisplayName = 'Password Reset Started'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'TroubleSigningIn'
Set @ActionName = 'PasswordResetFailed'
Set @ModuleDisplayName = 'Trouble Signing In'
Set @ActionDisplayName = 'Password Reset Failed'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'TroubleSigningIn'
Set @ActionName = 'PasswordReset'
Set @ModuleDisplayName = 'Trouble Signing In'
Set @ActionDisplayName = 'Password Reset'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Access'
Set @ActionName = 'Register'
Set @ModuleDisplayName = 'Access'
Set @ActionDisplayName = 'Register'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SubAccountSettings'
Set @ActionName = 'Change'
Set @ModuleDisplayName = 'Account Settings'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SupportSignOn'
Set @ActionName = 'Start'
Set @ModuleDisplayName = 'Support SignOn'
Set @ActionDisplayName = 'Start'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'SupportSignOn'
Set @ActionName = 'End'
Set @ModuleDisplayName = 'Support SignOn'
Set @ActionDisplayName = 'End'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Mfa'
Set @ActionName = 'DebugPasscodeRequested'
Set @ModuleDisplayName = 'Mfa'
Set @ActionDisplayName = 'Debug Passcode Requested'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'Mfa'
Set @ActionName = 'DebugPasscodeUsed'
Set @ModuleDisplayName = 'Mfa'
Set @ActionDisplayName = 'Debug Passcode Used'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'MobileDeposit'
Set @ActionName = 'DepositComplete'
Set @ModuleDisplayName = 'Mobile Deposit'
Set @ActionDisplayName = 'Check Deposited'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'MemberToMember'
Set @ActionName = 'AddRecipient'
Set @ModuleDisplayName = 'Recipient'
Set @ActionDisplayName = 'Created'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'MemberToMember'
Set @ActionName = 'EditRecipient'
Set @ModuleDisplayName = 'Recipient'
Set @ActionDisplayName = 'Updated'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'MemberToMember'
Set @ActionName = 'DeleteRecipient'
Set @ModuleDisplayName = 'Recipient'
Set @ActionDisplayName = 'Deleted'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'MemberToMember'
Set @ActionName = 'Transfer'
Set @ModuleDisplayName = 'MemberToMember'
Set @ActionDisplayName = 'Transfer'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'BillPayPayee'
Set @ActionName = 'AddPayee'
Set @ModuleDisplayName = 'BillPayPayee'
Set @ActionDisplayName = 'Created'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'BillPayPayment'
Set @ActionName = 'Scheduled'
Set @ModuleDisplayName = 'BillPayPayment'
Set @ActionDisplayName = 'Scheduled'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'External Transfers'
Set @ActionName = 'Micro-deposits Resent'
Set @ModuleDisplayName = 'External Transfers'
Set @ActionDisplayName = 'Micro-deposits Resent'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'External Transfers'
Set @ActionName = 'Created External Account'
Set @ModuleDisplayName = 'External Transfers'
Set @ActionDisplayName = 'Created External Account'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'External Transfers'
Set @ActionName = 'Edited Account Details'
Set @ModuleDisplayName = 'External Transfers'
Set @ActionDisplayName = 'Edited Account Details'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'External Transfers'
Set @ActionName = 'Removed External Account'
Set @ModuleDisplayName = 'External Transfers'
Set @ActionDisplayName = 'Removed External Account'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'External Transfers'
Set @ActionName = 'Verified Deposits'
Set @ModuleDisplayName = 'External Transfers'
Set @ActionDisplayName = 'Verified Deposits'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'TroubleSigningIn'
Set @ActionName = 'UsernameReset'
Set @ModuleDisplayName = 'Trouble Signing In'
Set @ActionDisplayName = 'Username Reset'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran

Set @ModuleName = 'TroubleSigningIn'
Set @ActionName = 'UsernameResetStarted'
Set @ModuleDisplayName = 'Trouble Signing In'
Set @ActionDisplayName = 'Username Reset Started'

begin tran
if not exists (select * from ActivityLogLabels with (updlock,serializable) where ModuleName = @ModuleName and ActionName = @ActionName)
begin
    insert into ActivityLogLabels (InsertAt, UpdateAt, ExpireAt, ExtendData, ModuleName, ActionName, ModuleDisplayName, ActionDisplayName)
    values (GetDate(), GetDate(), Null, Null, @ModuleName, @ActionName, @ModuleDisplayName, @ActionDisplayName)
end
commit tran


