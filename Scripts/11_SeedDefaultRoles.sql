

--Seed Default User Role

Declare @RoleId nvarchar(450), @PermissionId bigint

IF NOT EXISTS (SELECT * FROM DigitalRoles WITH (updlock,serializable) WHERE IsAdmin = 0 AND IsDefault = 1)
BEGIN
	BEGIN TRAN
	Set @RoleId = 'DAFB63DC-A3E7-43DB-8253-F79682715ED7'
	INSERT INTO DigitalRoles
	(Id, CreatedAt, InsertAt, UpdateAt, Name, NormalizedName, Description, ConcurrencyStamp, IsAdmin, CreatedBy, IsDefault)
	VALUES
	(@RoleId, GETUTCDATE(),GETUTCDATE(),GETUTCDATE(),'User_Default', 'USER_DEFAULT', 'Default End User Permissions', '93F09355-EB10-4CF4-A849-AA0CC35A377E', 0, 'System', 1)

	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Account_UpdateSettings')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Account_UpdateSortOrder')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Account_UpdateFavorite')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'AccountSummary_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'AccountSummary_View')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Cards_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Cards_UpdateTravelNotice')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Cards_ChangeCardPin')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Cards_ChangeCardStatus')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'CheckImages_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Credentials_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Credentials_ChangePassword')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Credentials_ChangeUsername')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Devices_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Devices_AddOrUpdate')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Devices_EnableBiometricAuthentication')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Devices_DisableBiometricAuthentication')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Documents_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Documents_UpdateStatementSettings')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'LoanPayoff_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'LoanPayoff_Calculate')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_AddAddress')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_EditAddress')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_DeleteAddress')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_AddPhoneNumber')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_EditPhoneNumber')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_DeletePhoneNumber')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_AddEmailAddress')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_EditEmailAddress')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'ContactInformation_DeleteEmailAddress')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'UserActivity_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'UserSettings_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'UserSettings_UpdateTimeout')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'SecureMessages_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'SecureMessages_SendMessage')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'SecureMessages_DeleteMessage')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'StopPayments_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'StopPayments_Add')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'StopPayments_Update')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'StopPayments_Delete')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'StopPayments_Eligible')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Transfer_TransferFrom')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Transfer_TransferTo')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Transfer_SchTransferFrom')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Transfer_SchTransferTo')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Transfer_View')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'Transfer_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'TransactionHistory_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'TransactionHistory_Eligible')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'TransactionHistoryExport_Eligible')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'TransactionHistoryExport_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END

	COMMIT TRAN
END


--Seed Default Admin Role
				
IF NOT EXISTS (SELECT * FROM DigitalRoles WITH (updlock,serializable) WHERE IsAdmin = 1 AND IsDefault = 1)
BEGIN
	BEGIN TRAN
	Set @RoleId = 'FED25009-8ADC-43AA-AA75-CA87F973D59D'
	INSERT INTO DigitalRoles
	(Id, CreatedAt, InsertAt, UpdateAt, Name, NormalizedName, Description, ConcurrencyStamp, IsAdmin, CreatedBy, IsDefault)
	VALUES
	(@RoleId, GETUTCDATE(),GETUTCDATE(),GETUTCDATE(), 'Admin_Default', 'ADMIN_DEFAULT', 'Default Admin User Permissions', '37197452-F1DE-4F1A-94DF-FE8C225BDD9C', 1, 'System', 1)

	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'EndUserManagement_Access')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'EndUserManagement_ViewEndUser')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'EndUserManagement_ManageEndUser')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'EndUserManagement_SupportSignOn')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'EndUserManagement_ViewGlobalEndUserActivityLog')
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END
	SET @PermissionId = (SELECT Id FROM FeaturePermissions WHERE FullyQualifiedName = 'SkipPay_Access')
	print '@PermissionId'
	print @PermissionId
	IF NOT EXISTS (SELECT * FROM DigitalRolePermissions WHERE @PermissionId IS NULL OR (FeaturePermissionId = @PermissionId AND DigitalRoleId = @RoleId))
	BEGIN
		print 'insert'
		INSERT INTO DigitalRolePermissions
		(InsertAt, UpdateAt, FeaturePermissionId, DigitalRoleId, Allowed)
		VALUES
		(GETUTCDATE(),GETUTCDATE(),@PermissionId,@RoleId,1)
	END

	COMMIT TRAN
END