BEGIN TRAN
update Configurations set data = replace(data,'Debug":false,','Debug":true,')
where id in (select id from dbo.Configurations where PropertyNames = 'debug')
update Configurations set data='{"FeeAmount":2.0,"FeeAmountForCheckRange":2.0,"GLCode":null,"ExpirationDays":180,"ACHStop":false,"ManageStop":true,"Memo":true,"Id":53,"InstanceName":"default","DisplayName":"Stop Payments Configuration","Category":"Account Services","Description":"Stop payment configuration","IsImplementationSetting":false}' where DisplayName='Stop Payments Configuration'
update Configurations set data='{"SyncWithCore":true,"MicroDepositExpirationDays":5,"VerificationDelayInHours":6,"MicroDepositMinimum":1,"MicroDepositMaximum":99,"LoanPaymentsAllowed":true,"Debug":true,"Id":33,"InstanceName":"default","DisplayName":"External Transfers","Category":"Move Money Settings","Description":"Configuration settings for external account and transfer management","IsImplementationSetting":false}' where DisplayName='External Transfers'
update Configurations set data='{"Enabled":true,"EmailChangeTrigger":true,"PhoneNumberChangeTrigger":true,"AddressChangeTrigger":true,"UsernameChangeTrigger":true,"PasswordChangeTrigger":true,"ExternalTransferTrigger":true,"ExternalTransferThresholdPerItem":0.0,"BillPayTrigger":true,"BillPayThresholdPerItem":0.0,"CheckRequestTrigger":true,"CheckRequestThresholdPerItem":0.0,"Id":23,"InstanceName":"default","DisplayName":"OTP Triggers","Category":"General","Description":"Passcode triggers for high-risk/fraud prevention.","IsImplementationSetting":false}' where DisplayName='OTP Triggers'
update Configurations set data='{"FeeAmount":2.0,"FeeAmountForCheckRange":2.0,"GLCode":"0","ExpirationDays":180,"ACHStop":true,"ManageStop":true,"Memo":true,"Id":52,"InstanceName":"default","DisplayName":"Stop Payments Configuration","Category":"Account Services","Description":"Stop payment configuration","IsImplementationSetting":false}' where DisplayName='Stop Payments Configuration'
Update Features set Enabled=1 where Name in('skippay', '')
Update Features set Enabled=1 where Name in('Billpay', '')
Update Features set Enabled=1 where Name in('Profile', '')
Update Features set Enabled=1 where Name in('Goals', '')
Update Features set Enabled=1 where Name in('Categorization', '')
Update Features set Enabled=1 where Name in('PersonalFinanceManagement', '')
Update Features set Enabled=1 where Name in('UserSettings', '')
IF NOT EXISTS(SELECT 1 FROM [TermsAndConditions] WHERE FeatureName='Registration')
BEGIN
INSERT INTO [TermsAndConditions]([FeatureName]
      ,[RequiresAcceptance]
      ,[Text])
	  VALUES('Registration', 1, 'test terms and conditions');
END

IF NOT EXISTS(SELECT 1 FROM [TermsAndConditions] WHERE FeatureName='skippay')
BEGIN
INSERT INTO [TermsAndConditions]([FeatureName]
      ,[RequiresAcceptance]
      ,[Text])
	  VALUES('skippay', 1, 'test terms and conditions');
END

IF NOT EXISTS(SELECT 1 FROM [TermsAndConditions] WHERE FeatureName='BillPay')
BEGIN
INSERT INTO [TermsAndConditions]([FeatureName]
      ,[RequiresAcceptance]
      ,[Text])
	  VALUES('BillPay', 1, 'test terms and conditions');
END

declare @roleId  varchar(50)
select top 1 @roleId =id  from DigitalRoles where IsDefault=1 and IsAdmin=0

  insert into [DigitalRolePermissions](DigitalRoleId,
FeaturePermissionId,
Allowed)
  select @roleId as DigitalRoleId, id, 1 from FeaturePermissions where id not in(SELECT TOP (1000)
      [FeaturePermissionId]
  FROM [DigitalRolePermissions]
  where [DigitalRoleId]=@roleId)
  
IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'FeatureContextId'
          AND Object_ID = Object_ID(N'dbo.subaccountmetadata'))
BEGIN
    ALTER TABLE subaccountmetadata
DROP COLUMN FeatureContextId;
END	  
IF NOT EXISTS (SELECT 1 FROM [TermsLink] where id=(select Id from TermsAndConditions where FeatureName = 'Registration') )
BEGIN
INSERT INTO [TermsLink]
           ([TermsAndConditionsId]
           ,[Href]
           ,[DisplayText])
     VALUES
           ((select Id from [TermsAndConditions] where FeatureName = 'Registration')
           ,'https://alogent.com'
           ,'Terms and Conditions')
END

IF NOT EXISTS (SELECT 1 FROM [TermsLink] where id=(select Id from TermsAndConditions where FeatureName = 'BillPay') )
BEGIN
INSERT INTO [TermsLink]
           ([TermsAndConditionsId]
           ,[Href]
           ,[DisplayText])
     VALUES
           ((select Id from [TermsAndConditions] where FeatureName = 'BillPay')
           ,'https://alogent.com'
           ,'Terms and Conditions')
END 

declare @userId NVARCHAR(900),@portfolioId  INT
select @userId=id,@portfolioId=PortfolioId from DigitalUsers where username='auto_profile'
INSERT INTO [dbo].[UserVendorData]
           ([ExtendData],[IntegrationKey],[UserId],[PortfolioId],[CoreKey])
     VALUES
           ('{"UserId":"'+@userId+'"}','BillPay.Mock',@userId,@portfolioId,'0000012345')
declare @vendorId BIGINT
select @vendorId=id from UserVendorData where PortfolioId=@portfolioId
INSERT INTO [dbo].[BillPayProfile]
           ([VendorDataId],[Name],[PhoneNumber],[EmailAddress],[AddressLine1],[AddressLine2],[City],[State],[PostalCode])
     VALUES
           (@vendorId,'Andrew J Smith','2343243243','test@tes.com','test','test2','Atlanta','GA','12345')

select @userId=id,@portfolioId=PortfolioId from DigitalUsers where username='auto_billpay'
INSERT INTO [dbo].[UserVendorData]
           ([ExtendData],[IntegrationKey],[UserId],[PortfolioId],[CoreKey])
     VALUES
           ('{"UserId":"'+@userId+'"}','BillPay.Mock',@userId,@portfolioId,'0000012345')
select @vendorId=id from UserVendorData where PortfolioId=@portfolioId
INSERT INTO [dbo].[BillPayProfile]
           ([VendorDataId],[Name],[PhoneNumber],[EmailAddress],[AddressLine1],[AddressLine2],[City],[State],[PostalCode])
     VALUES
           (@vendorId,'Andrew J Smith','2343243243','test@tes.com','test','test2','Atlanta','GA','12345')
select top 1 @portfolioId =PortfolioId  from DigitalUsers where UserName='auto_profile'	   
INSERT INTO [dbo].[EmailAddresses]
           ([PortfolioId],[Type],[Primary],[Email],[Verified])
     VALUES
           (@portfolioId, 'Personal',1,'test@test.com',0)
INSERT INTO [dbo].[PhoneNumbers]
           ([PortfolioId],[Primary],[Type],[Number],[Verified])
     VALUES
           (@portfolioId,1,'Home Phone','1111111112',0)	

INSERT INTO [dbo].[Addresses]
           ([PortfolioId],[Type],[Primary],[AddressLine1],[AddressLine2],[City],[StateCode],[Country],[CountryCode],[PostalCode],[Verified],[IsInternational])
     VALUES
           (@portfolioId,'0',1,'sample address1','sample address2','Atlanta','GA','United States','US','12345',0,0)

COMMIT TRAN